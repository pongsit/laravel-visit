<?php

namespace Pongsit\Visit;

use GuzzleHttp\Middleware;
use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;
use Pongsit\Visit\Providers\EventServiceProvider;

class VisitServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        // $this->mergeConfigFrom(
        //     __DIR__.'/../config/role.php', 'services'
        // );
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app['router']->namespace('Pongsit\\Visit\\Controllers')
                ->middleware(['web'])
                ->group(function () {
                    $this->loadRoutesFrom(__DIR__ . '/../routes/web.php');
                });
        // $this->app['router']->aliasMiddleware('isAdmin', IsAdmin::class);
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');
        $this->loadViewsFrom(__DIR__.'/../resources/views', 'visit');
        $this->publishes([
            __DIR__.'/../config/visit.php' => config_path('visit.php'),
        ], 'public');
    }
}