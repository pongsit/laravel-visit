<?php

namespace Pongsit\Visit\Http\Controllers;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Response;
use Pongsit\User\Models\User;
use Throwable;
use Image;
use Pongsit\visit\Models\Visit;
use Pongsit\System\Models\System;

class VisitController extends Controller
{
	public function index(){
		// $variables['abilities'] = Visit::where('status',1)->orderBy('power','desc')->get();
		// return view('Visit::index',$variables);
	}

	public function store(Request $request)
    {

        $request->validate([
            'name' => 'required',
        ],[
            'name.required'=>'กรุณากรอกชื่อแสดง',
        ]);

        $infos = [];
        $infos['name'] = $request->name;

        if(!empty($request->description)){
            $infos['description'] = $request->description;
        }

        if(isset($request->status)){
            $infos['status'] = $request->status;
        }

        if(!empty($request->id)){
            $visit = Visit::find($request->id);
            $visit->update($infos);
        }else{
        	$visit = new visit($infos);
            $visit->unique = unique();
            $visit->slug = slug($visit, 'unique');
            $visit->save();
        }

        return back()->with(['success'=>'เรียบร้อย']);
    }

}
