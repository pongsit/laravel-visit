<?php

namespace Pongsit\Visit\Models;

use Pongsit\Visit\Database\Factories\VisitFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Pongsit\Visit\Models\Statement;

class Visit extends Model
{
  use HasFactory;
  
  // Disable Laravel's mass assignment protection
  protected $guarded = [];

  // ไว้บันทึก Visitable_type และ Visitable_id
  public function visitable()
  {
      return $this->morphTo();
  }

}
