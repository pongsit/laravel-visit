@props(['statement'=>'','id'=>$id,'title'=>$title])

<div class="modal fade" id="{{$id}}" tabindex="-1" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">{{$title}}</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{route('statement.store')}}">
          @csrf
          <div class="d-flex mb-2">
            <div style="width:80px;">รายละเอียด*:</div>
            <div class="flex-fill">
              <input type="text" name="detail" class="form-control" placeholder="เช่น รับค่าเช่า" value="{{$statement->detail ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2">
            <div style="width:80px;">รูปแบบ*:</div>
            <div class="flex-fill">
              <div class="form-check d-inline-block">
                <label class="form-check-label">
                <input type="radio" class="credit_radio" checked="" >
                รายรับ
                </label>
                <label class="form-check-label">
                <input type="radio" class="debit_radio">
                รายจ่าย
                </label>
              </div>
              <script>
                $(function(){
                  $('.credit_form').hide();
                  $('.debit_form').hide();
                  $('body').on('click tap','.statement_type',function(){
                    console.log('hi');
                  });
                });
              </script>
            </div>
          </div>
          <div class="d-flex mb-2 credit_form">
            <div style="width:80px;">รายรับ</div>
            <div class="flex-fill">
              <input type="text" name="credit" class="form-control" placeholder="เช่น 1000" value="{{$statement->credit ?? ''}}">
            </div>
          </div>
          <div class="d-flex mb-2 debit_form">
            <div style="width:80px;">รายจ่าย:</div>
            <div class="flex-fill">
              <input type="text" name="debit" class="form-control" placeholder="เช่น 1000" value="{{$statement->debit ?? ''}}">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">ยกเลิก</button>
        <button type="button" class="btn btn-primary {{$id}}-submit">ส่งข้อมูล</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(function () {
        $('body').on('click', '.{{$id}}-submit', function (e) {
            $('#{{$id}}').find('form').submit();
            $('#{{$id}}').modal('hide');
        });
    });
</script>