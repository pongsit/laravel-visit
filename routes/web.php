<?php

use Illuminate\Support\Facades\Route;
use Pongsit\Visit\Http\Controllers\VisitController;

Route::prefix('visit')->middleware(['userCan:manage_visit'])->group(function () {
	Route::post('/store', [VisitController::class, 'store'])->name('visit.store');
});